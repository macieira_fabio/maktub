import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AgendamentoComponent } from './agendamento/agendamento.component';
import { ClienteComponent } from './cliente/cliente.component';
import { DashBoardComponent } from './dash-board/dash-board.component';

const routes: Routes = [
  {path: '', redirectTo: 'dashBoard', pathMatch: 'full' },
  {path: 'dashBoard', component: DashBoardComponent},
  {path: 'cliente', component: ClienteComponent},
  {path: 'agendamento', component: AgendamentoComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }


