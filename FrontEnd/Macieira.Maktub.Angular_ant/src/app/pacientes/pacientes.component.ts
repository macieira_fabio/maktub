import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

import { Paciente } from '../Models/paciente';

import { PacienteService } from './Paciente.service';

@Component({
  selector: 'app-pacientes',
  templateUrl: './pacientes.component.html',
  styleUrls: ['./pacientes.component.css']
})
export class PacientesComponent implements OnInit {

   titulo = 'Pacientes';
   public  modo = 'post';
   
   public modalRef: BsModalRef;
   public pacienteForm: FormGroup;
   public pacienteSelecionado: Paciente;

   public pacientes: Paciente[];

   constructor(private fb: FormBuilder, 
                private modalService: BsModalService,
                private pacienteService: PacienteService) {
      this.criarForm();
    }

    ngOnInit(): void {
      this.carregarPaciente();
    }

    carregarPaciente(){
      this.pacienteService.getAll().subscribe(
        (paciente: Paciente[]) => {
          this.pacientes = paciente;
        },
        (erro: any) => {
          console.error(erro);
        },
      );
      console.log(this.pacientes)
    }
    
    criarForm() {
      this.pacienteForm = this.fb.group({
        id: [''],
        nome: ['', Validators.required], 
        sobreNome: ['', Validators.required],
        telefone: ['', Validators.required]
      });
    }

    salvarPaciente(paciente: Paciente){
      (paciente.id === 0) ? this.modo = 'post' : this.modo = 'put';
      this.pacienteService[this.modo](paciente).subscribe(
      (retorno: Paciente) => { 
        console.log(retorno);
        this.carregarPaciente();
        this.pacienteForm.reset();
      },
      (erro: any ) => {
        console.error(erro);
        });
    }

    deletarPaciente(id: number){
      this.pacienteService.delete(id).subscribe(
        (model: any) => {
          console.log(model);
          this.carregarPaciente();
        },
        (erro: any) => {
          console.error(erro);
        },
      );
    }

    pacienteSubmit() {
      this.salvarPaciente(this.pacienteForm.value);
    };

    pacienteSelect(paciente: Paciente) {
      this.pacienteSelecionado = paciente;
      this.pacienteForm.patchValue(paciente);
    };

    pacienteNovo(){
      this.pacienteSelecionado = new Paciente();
      this.pacienteForm.patchValue(this.pacienteSelecionado)
    }

    voltar() {
      this.pacienteSelecionado = null;
      this.pacienteForm.reset();
    };

    openModal(template: TemplateRef<any>) {
      this.modalRef = this.modalService.show(template);
     }

}