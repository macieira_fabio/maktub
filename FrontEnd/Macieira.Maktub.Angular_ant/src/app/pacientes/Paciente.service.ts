import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Paciente } from '../Models/paciente';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.urlPrincipal}/api/paciente`;
  
    getAll(): Observable<Paciente[]> {
      return this.http.get<Paciente[]>(`${this.baseUrl}`);
    }
  
    post(paciente: Paciente ){ 
      return this.http.post(`${this.baseUrl}`, paciente);
    }
  
    put(paciente: Paciente){ 
      return this.http.put(`${this.baseUrl}/${paciente.id}`, paciente);
    }
  
    delete(id: number): Observable<Paciente>{
      return this.http.delete<Paciente>(`${this.baseUrl}/${id}`);
    }
  }
  