import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Consulta } from '../Models/Consulta';
import { Medico } from '../Models/Medico';
import { MedicoService } from './Medico.service';

@Component({
  selector: 'app-medico',
  templateUrl: './medico.component.html',
  styleUrls: ['./medico.component.css']
})
export class MedicoComponent implements OnInit {

  titulo = "Medicos";
  public modo = 'post';
  public consultasPorMedico: Consulta[];
  public medicoForm: FormGroup;
  public modalRef: BsModalRef;
  public medicoSelecionado: Medico;
  public medicos: Medico[];
  
  
  openModal(template: TemplateRef<any>, medicoId: number){
        
    this.medicoService.GetByPacientebyMedicoId(medicoId)
    .subscribe(
    (resp) => {
      console.log(resp);
      this.consultasPorMedico = resp[0].consulta;
    },
    (erro: any) => {
      console.error(erro);
    },
    )
    this.modalRef = this.modalService.show(template);
  }

  constructor(private fb: FormBuilder,
              private modalService: BsModalService,
              private medicoService: MedicoService) {

    this.criarForm();
   }

  ngOnInit(): void {
    this.carregatMedico();
  }

  carregatMedico(){
    this.medicoService.getAll()
      .subscribe(
        (retorno: Medico[]) => {
          this.medicos = retorno
        },
        (erro: any) => {
          console.error(erro);
        },
      );
      console.log(this.medicos)
  }

  criarForm(){
    this.medicoForm = this.fb.group({
      id: ['', Validators.required],
      nome: ['', Validators.required],
      especialidade: ['', Validators.required],
      crm: ['', Validators.required]
    });
  }

  salvaMedico(medico: Medico){
    (medico.id === 0) ? this.modo = 'post' : this.modo = 'put';
    this.medicoService[this.modo](medico).subscribe(
      (retorno: Medico) => {
        console.log(retorno);
        this.carregatMedico();
        this.medicoForm.reset();
      },
      (erro: any) => {
        console.error(erro);
      },
    );
  }

  deletearMedico(id: number){
    this.medicoService.delete(id).subscribe(
      (model: any) => {
        console.log(model);
        this.carregatMedico();
        this.criarForm();
      },
      (erro: any) => {
        console.error(erro);
      },
    );
  }

  medicoSubmit(){
    this.salvaMedico(this.medicoForm.value);
  }

  medicoSelect(medico: Medico){
    this.medicoSelecionado = medico;
    this.medicoForm.patchValue(medico);
  }

  medicoNovo(){
    this.medicoSelecionado = new Medico();
    this.medicoForm.patchValue(this.medicoSelecionado)
  }

  voltar(){
    this.medicoSelecionado = null;
  }

}
