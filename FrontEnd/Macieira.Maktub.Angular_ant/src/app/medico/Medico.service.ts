import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Medico } from '../Models/Medico';

@Injectable({
  providedIn: 'root'
})
export class MedicoService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.urlPrincipal}/api/medico`;
  
    getAll(): Observable<Medico[]> {
      return this.http.get<Medico[]>(`${this.baseUrl}`);
    }
  
    GetByPacientebyMedicoId(id: number){ 
      return this.http.get<Medico[]>(`${this.baseUrl}/byMedico/${id}` );
    }

    post(medico: Medico){ 
      return this.http.post(`${this.baseUrl}`, medico);
    }
  
    put(medico: Medico){ 
      return this.http.put(`${this.baseUrl}/${medico.id}`, medico);
    }
  
    delete(id: number): Observable<Medico>{
      return this.http.delete<Medico>(`${this.baseUrl}/${id}`);
    }
  }
  