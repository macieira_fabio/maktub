import { Consulta } from './Consulta';

export class Medico {

    constructor() {
        this.id = 0;
        this.nome = null;
        this.especialidade = null;
        this.crm = null;        
    }

    id: number;
    nome: string;
    especialidade: string;
    crm: string;
    consulta: Consulta[];
}