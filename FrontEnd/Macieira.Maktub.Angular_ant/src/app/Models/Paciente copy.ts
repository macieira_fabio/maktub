export class Cliente {
    
    constructor() {
        this.id = 0;
        this.nome = null;
        this.sobreNome = null;
        this.telefone = "";        
    }

    id: number;
    nome: string;
    sobreNome: string;
    telefone: string;
}
