import { Paciente } from './paciente';

export class Consulta{
    
    constructor() {
        this.id = 0;
        //this.paciente = new Paciente();
        this.especialidade = '';
        this.medicoId = 0;
        this.dataDaConsulta = '';
    }

    id: number;
    pacienteId: number;
    especialidade: string;
    medicoId: number;
    dataDaConsulta: string;
}