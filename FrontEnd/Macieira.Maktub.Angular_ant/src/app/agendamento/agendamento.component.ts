import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MedicoService } from '../medico/Medico.service';
import { Consulta } from '../Models/Consulta';
import { PacienteService } from '../pacientes/Paciente.service';
import { AgendamentoService } from './agendamento.service';
import { defineLocale, ptBrLocale } from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Paciente } from '../Models/paciente';

defineLocale('pt-br', ptBrLocale)

@Component({
  selector: 'app-agendamento',
  templateUrl: './agendamento.component.html',
  styleUrls: ['./agendamento.component.css']
})
export class AgendamentoComponent implements OnInit {

  public titulo = 'Marcação de Consultas'
  public modo = 'post';
  notification = null;

  public consultaForm : FormGroup;
  public consultas: Consulta[];
  public pacienteList = [];
  public medicoList =  [];

  public consultaSelecionada: Consulta;
    
  constructor(private fb: FormBuilder,
              private localeService: BsLocaleService,
              private consultaService: AgendamentoService,
              private pacientesService: PacienteService,
              private medicoService: MedicoService) { 
    this.criarForm();
    this.localeService.use('pt-br'); 
    }

  ngOnInit(): void {
    this.carregarConsulta();

    this.pacientesService.getAll().subscribe(res => this.pacienteList = res as []);
    this.medicoService.getAll().subscribe(res => this.medicoList = res as []);

    console.log(this.pacienteList.values);
  }

  criarForm(){
    this.consultaForm = this.fb.group({
      id: [''],
      pacienteId:[null, [Validators.required]],
      especialidade: ['', Validators.required],
      medicoId: ['', Validators.required],
      dataDaConsulta: ['', Validators.required]
    });
  }

  carregarConsulta(){
    this.consultaService.getAll().subscribe(
      (consulta: Consulta[]) =>{
          this.consultas = consulta;
      },
      (erro: any) => {
        console.log(erro);
      });
  }

  salvaAgendamento(consulta: Consulta){
    (consulta.id === 0) ? this.modo = 'post' : this.modo = 'put';
    this.consultaService[this.modo](consulta).subscribe(
      (retorno: Consulta) => {
         console.log(retorno);
         this.carregaConsultas();
         this.consultaForm.reset();
      },
      (erro: any) => {
        console.error(erro);
      });
  }

  carregaConsultas(){
    this.consultaService.getAll()
      .subscribe(
        (retorno: Consulta[]) => {
          this.consultas = retorno
        },
        (erro: any) => {
          console.error(erro);
        },
      );
      console.log(this.consultas)
  }

  consultaSubmit(){
    this.salvaAgendamento(this.consultaForm.value);
  }

  consultaSelect(consulta: Consulta){
    this.consultaSelecionada = consulta;
    this.consultaForm.patchValue(consulta);
  }

  ConsultaNovo(){
    this.consultaSelecionada = new Consulta();
    this.consultaForm.patchValue(this.consultaSelecionada);
  }

  deletarConsulta(id: number){
    this.consultaService.delete(id).subscribe(
      (model: any) => {
        console.log(model);
        this.carregaConsultas();
      },
      (erro: any) => {
        console.error(erro);
      },
    );
  }

  voltar(){
    this.consultaSelecionada = null;
    this.consultaForm.reset();
  }

}
