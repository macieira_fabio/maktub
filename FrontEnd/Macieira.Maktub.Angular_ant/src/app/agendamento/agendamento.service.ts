import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Consulta } from '../Models/Consulta';


@Injectable({
  providedIn: 'root'
})
export class AgendamentoService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.urlPrincipal}/api/consulta`;
  
    getAll(): Observable<Consulta[]> {
      return this.http.get<Consulta[]>(`${this.baseUrl}`);
    }
    
    post(consulta: Consulta ){ 
      return this.http.post(`${this.baseUrl}`, consulta);
    }
  
    put(consulta: Consulta){ 
      return this.http.put(`${this.baseUrl}/${consulta.id}`, consulta);
    }
  
    delete(id: number): Observable<Consulta>{
      return this.http.delete<Consulta>(`${this.baseUrl}/${id}`);
    }
  }
  