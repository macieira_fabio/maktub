import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Cliente } from '../Models/Paciente copy';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  constructor(private http: HttpClient) { }

  baseUrl = `${environment.urlPrincipal}/api/cliente`;
  
    getAll(): Observable<Cliente[]> {
      return this.http.get<Cliente[]>(`${this.baseUrl}`);
    }
  
    GetByPacientebyClienteId(id: number){ 
      return this.http.get<Cliente[]>(`${this.baseUrl}/byCliente/${id}` );
    }

    post(cliente: Cliente){ 
      return this.http.post(`${this.baseUrl}`, cliente);
    }
  
    put(cliente: Cliente){ 
      return this.http.put(`${this.baseUrl}/${cliente.id}`, cliente);
    }
  
    delete(id: number): Observable<Cliente>{
      return this.http.delete<Cliente>(`${this.baseUrl}/${id}`);
    }
  }
