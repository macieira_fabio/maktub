import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Cliente } from '../Models/Paciente copy';
import { ClienteService } from './Cliente.service';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html'  
})
export class ClienteComponent implements OnInit {

  titulo = "Clientes";
  public modo = 'post';
  public consultasPorCliente: Cliente[];
  public clienteForm: FormGroup;
  public modalRef: BsModalRef;
  public clienteSelecionado: Cliente;

  public clientes: Cliente[];
  
   
  constructor(private fb: FormBuilder,
              private ClienteService: ClienteService) {

    this.criarForm();
   }

  ngOnInit(): void {
    this.carregatCliente();
  }

  carregatCliente(){
    this.ClienteService.getAll()
      .subscribe(
        (retorno: Cliente[]) => {
          this.clientes = retorno
        },
        (erro: any) => {
          console.error(erro);
        },
      );
      console.log(this.clientes)
  }

  criarForm(){
    this.clienteForm = this.fb.group({
      id: ['', Validators.required],
      nome: ['', Validators.required],
      especialidade: ['', Validators.required],
      crm: ['', Validators.required]
    });
  }

  salvaCliente(cliente: Cliente){
    (cliente.id === 0) ? this.modo = 'post' : this.modo = 'put';
    this.ClienteService[this.modo](cliente).subscribe(
      (retorno: Cliente) => {
        console.log(retorno);
        //this.toastr.success("Cliente salvo com Sucesso")
        this.carregatCliente();
        this.clienteForm.reset();
      },
      (erro: any) => {
        console.error(erro);
      },
    );
  }

  deletearCliente(id: number){
    this.ClienteService.delete(id).subscribe(
      (model: any) => {
        console.log(model);
        this.carregatCliente();
        this.criarForm();
      },
      (erro: any) => {
        console.error(erro);
      },
    );
  }

  ClienteSubmit(){
    this.salvaCliente(this.clienteForm.value);
  }

  clienteSelect(cliente: Cliente){
    this.clienteSelecionado = cliente;
    this.clienteForm.patchValue(cliente);
  }

  clienteNovo(){
    this.clienteSelecionado = new Cliente();
    this.clienteForm.patchValue(this.clienteSelecionado)
  }

  voltar(){
    this.clienteSelecionado = null;
  }

}

