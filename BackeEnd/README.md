# Sistema Maktub

## Tecnologias
A solução usa as seguintes tecnologias:

- C#
- Asp.Net Core
- Postgres
- TypeScript
- Angular

## Forma de entrega
Você deve criar um fork desse projeto e construir sua solução, ao terminar você precisa realizar um Pull request da sua branch develop para a branch develop desse repositorio.

#
## Informações

## Frontend
Este projeto foi gerado com[Angular CLI](https://github.com/angular/angular-cli) version 10.1.0, ngx-bootstrap e .NetCore version 3.1.401
## 

## Migrations
Execute `Donet ef migrations add initial`, o EF gerou o código que criará o banco de dados do zero. Esse código está na pasta migrações,cria as tabelas de banco de dados que correspondem aos conjuntos de entidades do modelo de dados ja defindo da DbContex, os dos criados serão iniciado com os id inicias de 100 populando a tabela e carregando a aplicação com registros existentes.

Obs: Ao inserir um novo registro sera inicinado com o id 1.

Execute `Donet migrations update database` A saída do comando é semelhante ao comando migrations add, exceto que os logs para os comandos SQL que configuram o banco de dados são exibidos. A maioria dos logs é omitida na seguinte saída de exemplo. Se você preferir não ver esse nível de detalhe em mensagens de log, altere o nível de log no arquivo appsettings.Development.json. 


## Banco de dados

Banco de dados esta configurado para excultar tanto com o `postgreSQL` quando o `Sqlite`. 

## Development server

Execute `nodemon ou ng serve` para um servidor de desenvolvimento. Navegar para `http://localhost:4200/`. O aplicativo será recarregado automaticamente se você alterar qualquer um dos arquivos de origem.

## Build

Execute `ng build` para construir o projeto. Os artefatos de construção serão armazenados no diretório `dist /`. Use o sinalizador `--prod` para um buil de produção

## WebApi 

A ferramenta swagger.json permite que eu vejo todos os endpoint e o contratos, execulte o Projeto WebApi e Acesse o endereço http://localhost:5000/. q