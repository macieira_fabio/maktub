﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Macieira.Maktub.Application.ViewModels.Base
{
    public class BaseViewModel
    {
        [Key]
        public int Id { get; set; }
        public DateTime DataInclusao { get; set; }
    }
}
