﻿namespace Macieira.Maktub.Application.ViewModels.Base
{
    public class ErrorViewModel : BaseViewModel
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }
    }
}
