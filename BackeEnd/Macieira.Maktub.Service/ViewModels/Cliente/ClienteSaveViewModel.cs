﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Macieira.Maktub.Service.ViewModels.Cliente
{
    public class ClienteSaveViewModel
    {
        [Required(ErrorMessage = "Nome é Obrigatorio")]
        [MinLength(3, ErrorMessage = "Mínimo 3 caracteres")]
        [MaxLength(100, ErrorMessage = "Máximo 100 catacteres")]
        [DisplayName("Nome")]
        public string Nome { get; set; }
    }
}
