﻿using Macieira.Maktub.Application.ViewModels.Base;
using System.ComponentModel.DataAnnotations;

namespace Macieira.Maktub.Service.ViewModels
{
    public class ClienteViewModel : BaseViewModel
    {
       
        [Required(ErrorMessage ="Nome é Obrigatorio")]
        [MinLength(5, ErrorMessage = "Mínimo 3 caracteres")]
        [MaxLength(100, ErrorMessage = "Maximo 100 Caracteres")]

        public string Nome { get; set; } 
    }
}
