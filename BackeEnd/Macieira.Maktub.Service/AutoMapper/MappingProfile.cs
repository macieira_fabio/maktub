﻿using AutoMapper;
using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.Service.ViewModels;
using Macieira.Maktub.Service.ViewModels.Cliente;

namespace Macieira.Maktub.Service.AutoMapper
{
    public class MappingProfile  : Profile
    {
        public MappingProfile()
        {
            CreateMap<Cliente, ClienteViewModel>().ReverseMap();
            CreateMap<ClienteViewModel, ClienteSaveViewModel>().ReverseMap();
        }
    }
}
