﻿using AutoMapper;
using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.Domain.Interfaces.Repositories;
using Macieira.Maktub.Domain.Validators;
using Macieira.Maktub.Service.ViewModels;
using Macieira.Maktub.Services;
using Macieira.Maktub.Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Macieira.Maktub.Service.Services
{
    public class ClienteAppService : BaseAppService<Cliente, ClienteViewModel, ClienteValidation>, IClienteAppService
    {
        private readonly IMapper mapper;
        private readonly IService<Cliente> service;
        public ClienteAppService(IMapper mapper, IService<Cliente> service)
            : base(mapper, service)
        {
            this.mapper = mapper;
            this.service = service;
        }

        public IEnumerable<ClienteViewModel> ObterClienteEspeciais(int id)
        {
            return mapper.Map<IEnumerable<ClienteViewModel>>(service.Select().Where(c => c.Id.Equals(id)).ToList());
        }

        public ClienteViewModel ObterClientePorId(int id)
        {
            return SelectLazyload().Where(p => p.Id.Equals(id)).FirstOrDefault();
        }

        public IEnumerable<ClienteViewModel> SelectLazyload()
        {
            //retornar todos os dados
            var query = CreateQuery();
            //    .Include(x => x.)
                //.Include(x => x.)
                //.AsNoTracking();

            return mapper.Map<IEnumerable<Cliente>, IEnumerable<ClienteViewModel>>(query.ToList());
        }
    }
}
