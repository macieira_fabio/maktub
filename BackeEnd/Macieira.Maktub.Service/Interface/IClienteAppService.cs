﻿using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.Domain.Validators;
using Macieira.Maktub.Service.ViewModels;
using System.Collections.Generic;

namespace Macieira.Maktub.Services.Interface
{
    public interface IClienteAppService : IAppService<Cliente, ClienteViewModel, ClienteValidation>
    {
        ClienteViewModel ObterClientePorId(int id);
        IEnumerable<ClienteViewModel> ObterClienteEspeciais(int id);
        IEnumerable<ClienteViewModel> SelectLazyload();

    }
}
       