﻿using FluentValidation;
using Macieira.Maktub.Application.ViewModels.Base;
using Macieira.Maktub.Domain.Entities;
using System.Collections.Generic;

namespace Macieira.Maktub.Services.Interface
{
    public interface IAppService<TEntity, VModel, Validator>
        where TEntity : Entity
        where VModel : BaseViewModel
        where Validator : AbstractValidator<TEntity>
    {
        VModel InsertOrUpdate(VModel vmodel);

        void Delete(int id);

        IEnumerable<VModel> Select();

        VModel SelectById(int id);

    }
}
