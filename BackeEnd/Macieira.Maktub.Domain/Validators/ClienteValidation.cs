﻿using FluentValidation;
using Macieira.Maktub.Domain.Entities;
using System;

namespace Macieira.Maktub.Domain.Validators
{
    public class ClienteValidation : AbstractValidator<Cliente>
    {
        public ClienteValidation()
        {
            RuleFor(c => c)
             .NotNull()
             .OnAnyFailure(x => {
                 throw new ArgumentNullException("Cliente nao encontrado");
             });

            //Verifica dados obrigatororios
            RuleFor(c => c.Nome)
                .NotEmpty()
                .NotNull()
                .WithMessage("Nome do Cliente Obrigatorio.");
        }

    }
}
