﻿using FluentValidation;
using Macieira.Maktub.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Macieira.Maktub.Domain.Interfaces.Repositories
{
    public interface IService<TEntity> where TEntity : Entity
    {
        TEntity Insert<V>(TEntity obj) where V : AbstractValidator<TEntity>;

        TEntity Update<V>(TEntity obj) where V : AbstractValidator<TEntity>;

        void Delete(int id);

        IList<TEntity> Select();

        TEntity SelectById(int id);

        IQueryable<TEntity> CreateQuery();
    }
}
