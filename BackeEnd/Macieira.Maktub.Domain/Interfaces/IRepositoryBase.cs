﻿using Macieira.Maktub.Domain.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Macieira.Maktub.Domain.Interfaces.Repositories
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        void Insert(TEntity obj);

        void Update(TEntity obj);

        void Delete(int id);

        IList<TEntity> Select();

        TEntity SelectById(int id);

        IQueryable<TEntity> CreateQuery();

        int SaveChanges();
    }
}
