﻿using System.Collections.Generic;

namespace Macieira.Maktub.Domain.Entities
{
    public class Email : Entity
    {
        public int EmailId { get; set; }
        public string Emails { get; set; }
        public int ClienteId { get; set; }

        public virtual IEnumerable<Cliente> Cliente { get; set; }
    }
}
