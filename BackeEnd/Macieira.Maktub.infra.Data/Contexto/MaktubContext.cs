﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.infra.Data.Mapping;
using System.Collections.Generic;

namespace Macieira.Maktub.infra.Data.Contexto
{
    public class MaktubContext : DbContext
    {
        private static bool dbCreated = false;

        #region -- Dbset --
        //public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        #endregion

        public MaktubContext()
        {
            if (!dbCreated)
            {
                dbCreated = true;
                //Database.EnsureDeleted();
                Database.EnsureCreated();
            }
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlite(config.GetConnectionString("DefaultConnection"));

                //Conexão como PostgreSql
                //"User ID=postgres;Password=123456;Server=localhost; Port=5432; Database=MyWebApi.dev; Integrated Security=true Pooling=true"
            }
        }

        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    #region -- Mepeamento --
        //    base.OnModelCreating(modelBuilder);

        //    modelBuilder.ApplyConfiguration(new ClienteMap()).Entity<Cliente>();

        //    modelBuilder.Entity<Cliente>()
        //        .HasData(new List<Cliente>(){
        //            new Cliente(1, "Maria Nara"),
        //            new Cliente(2, "João Fabio"),
        //            new Cliente(3, "Luis Paulo")
        //        });
        //    #endregion
        //}

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("DataInclusao") != null))
            {
                if (entry.State == EntityState.Added)
                {
                    entry.Property("DataInclusao").CurrentValue = DateTime.Now;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("DataInclusao").IsModified = false;
                }
            }
            return base.SaveChanges();
        }
    }

}
