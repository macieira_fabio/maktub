﻿using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.Domain.Interfaces.Repositories;
using Macieira.Maktub.infra.Data.Contexto;
using Macieira.Maktub.infra.Data.UoW;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Macieira.Maktub.infra.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly UnitOfWork unitOfWork;
        protected readonly DbSet<TEntity> DbSet;

        public Repository(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = (UnitOfWork)unitOfWork;
            DbSet = context.Set<TEntity>();
        }

        protected MaktubContext context { get { return unitOfWork.context; } }
        protected MaktubContext newcontext = new MaktubContext();

        public void Insert(TEntity obj)
        {
            DbSet.Add(obj);
        }
        public void Update(TEntity obj)
        {
            DbSet.Update(obj);
        }
        public void Delete(int id)
        {
            DbSet.Remove(DbSet.Find(id));
        }
        public IList<TEntity> Select()
        {
            //return DbSet.ToList();

            return this.newcontext.Set<TEntity>().AsNoTracking().ToList();

        }
        public TEntity SelectById(int id)
        {
            //return DbSet.Find(id);

            return this.newcontext.Set<TEntity>().Find(id);
        }
        public IQueryable<TEntity> CreateQuery()
        {
            return this.newcontext.Set<TEntity>().AsNoTracking();
        }
        public int SaveChanges()
        {
            return context.SaveChanges();
        }
        public void Disposes()
        {
            context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
