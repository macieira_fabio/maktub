﻿using Macieira.Maktub.Domain.Interfaces.Repositories;
using Macieira.Maktub.infra.Data.Contexto;

namespace Macieira.Maktub.infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        public MaktubContext context;

        public UnitOfWork(MaktubContext context)
        {
            this.context = context;
        }

        public bool Commit()
        {
            return context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
