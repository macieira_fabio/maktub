﻿using Macieira.Maktub.Domain.Entities;
using Macieira.Maktub.Domain.Interfaces.Repositories;
using Macieira.Maktub.infra.Data.Contexto;
using Macieira.Maktub.infra.Data.Repositories;
using Macieira.Maktub.infra.Data.UoW;
using Macieira.Maktub.Service.Services;
using Macieira.Maktub.Services;
using Macieira.Maktub.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Macieira.Maktub.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // AppService
            services.AddScoped<IClienteAppService, ClienteAppService>();

            // Service            
            services.AddScoped<IService<Cliente>, BaseService<Cliente>>();

            // Infra - Data  
            services.AddScoped<MaktubContext>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            // Infra - Data - Repository 
            services.AddScoped<IRepository<Cliente>, Repository<Cliente>>();

        }
    }
}
