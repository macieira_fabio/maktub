﻿using Macieira.Maktub.Domain.Interfaces.Repositories;
using Microsoft.AspNetCore.Mvc;


namespace Macieira.Maktub.WebApi.Controllers.Base
{
    public class BaseController : Controller
    {
        private IUnitOfWork unitOfWork;

        public BaseController(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public bool IsValidOperation()
        {
            return unitOfWork.Commit();
        }

    }
}
