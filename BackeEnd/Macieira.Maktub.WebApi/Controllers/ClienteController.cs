﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Macieira.Maktub.Domain.Interfaces.Repositories;
using Macieira.Maktub.Services.Interface;
using Macieira.Maktub.WebApi.Controllers.Base;
using Macieira.Maktub.WebApi.Extensions;
using Macieira.Maktub.WebApi.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Graph;

namespace Macieira.Maktub.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : BaseController
    {
        private readonly IMapper mapper;
        private readonly IClienteAppService appClienteService;

        public ClienteController(IUnitOfWork unitOfWork,
                                 IMapper mapper,
                                 IClienteAppService appClienteService) : base(unitOfWork)
        {
            this.mapper = mapper;
            this.appClienteService = appClienteService;
        }

        /// <summary>
        /// Retorna todos os clientes
        /// </summary>
        /// <returns code="200">A lista de de clientes foi retornada com sucesso</returns>
        /// <returns code="500">Erro ao retornar a lista de clientes</returns>
        [HttpGet()]
        public async Task<ActionResult<dynamic>> GetAll()
        {
            try
            {
                //var viewModel = appClienteService.SelectLazyload();
                var viewModel = appClienteService.Select();

                return Ok(new ResponseViewModel(viewModel));

            }
            catch (ServiceException e)
            {
                return StatusCode(HttpStatusCode.OK.ToInt(), new ResponseViewModel()
                {
                    HttpStatusCode = HttpStatusCode.OK,
                    ErrorMessage = e.Message
                });

            }
            catch (Exception ex)
            {
                return StatusCode(HttpStatusCode.InternalServerError.ToInt(), new ResponseViewModel()
                {
                    HttpStatusCode = HttpStatusCode.InternalServerError,
                    ErrorMessage = ex.Message
                });
            }
        }
    }
}