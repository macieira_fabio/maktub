﻿using System.Net;

namespace Macieira.Maktub.WebApi.Extensions
{
    public static class HttpStatusCodeExtensions
    {
        public static int ToInt(this HttpStatusCode httpStatusCode)
           => (int)httpStatusCode;
    }
}
