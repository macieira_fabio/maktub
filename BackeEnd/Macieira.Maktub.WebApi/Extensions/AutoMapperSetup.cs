﻿using AutoMapper;
using System;
using Microsoft.Extensions.DependencyInjection;
using Macieira.Maktub.Service.AutoMapper;
using System.Reflection;

namespace Macieira.Maktub.WebApi.Extensions
{
    public static class AutoMapperSetup
    {
        public static void AddAutoMapperSetup(this IServiceCollection services)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAutoMapper(typeof(Startup));

            // Registering Mappings automatically only works if the 
            // Automapper Profile classes are in ASP.NET project
            AutoMapperConfig.RegisterMappings();
        }
    }
}
