import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthLayoutRoutes } from './auth-layout.routing';
import { HttpClientModule } from "@angular/common/http";

import { AngularMaterialModule } from 'src/app/modules/shared/angular-material/angular-material.module';
import { AuthLayoutComponent } from './auth-layout.component';
import { LoginComponent } from 'src/app/modules/account/login/login.component';
import { RegisterUserComponent } from 'src/app/modules/account/register-user/register-user.component';
import { FlexLayoutModule } from '@angular/flex-layout';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AuthLayoutRoutes),
    FormsModule,
    HttpClientModule,
    AngularMaterialModule,
    ReactiveFormsModule,
    FlexLayoutModule,
  ],
  declarations: [AuthLayoutComponent, LoginComponent, RegisterUserComponent],
})
export class AuthLayoutModule {}
