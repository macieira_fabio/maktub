import { Routes } from '@angular/router';
import { LoginComponent } from 'src/app/modules/account/login/login.component';
import { RegisterUserComponent } from 'src/app/modules/account/register-user/register-user.component';

export const AuthLayoutRoutes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'registrar-usuario', component: RegisterUserComponent },
];
