import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AccountService } from '../shared/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  user = {
    email: '',
    password: ''
  }

  constructor(private accountServcie: AccountService, private router: Router) {}

  ngOnInit(): void {}

  onSubmit() {
    this.accountServcie.login(this.user);
    this.router.navigate(['']);
  }
}
